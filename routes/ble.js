var noble = require('noble');
// peripheral.once('connect', () => {
//     console.log("Connected.")
// });
// peripheral.once('servicesDiscover', services => {
//     console.log(service, "found")
// });
// service.once('includedServicesDiscover', (includedServ) => {
//     console.log(includedServ, "included serv")
// });
var measurementData;
var devices = [];
noble.on('discover', (peripheral) => {
    console.log(peripheral.advertisement.localName, "hehe")
    devices.push(peripheral);
    peripheral.once('connect', (data) => {
        //console.log("Connected.", data);
    });
    peripheral.once('servicesDiscover', (services) => {
        //console.log(services, "Services..")
        services.forEach(service => {
            if (service.name) {
                if (service.name.indexOf('Health') != -1)
                    service.once('characteristicsDiscover', (characteristics) => {
                        //console.log(characteristics);
                        characteristics.forEach(characteristic => {
                            if (characteristic.name)
                                if (characteristic.name.indexOf("Measurement") != -1) {
                                    console.log("Subscribing...");
                                    characteristic.on('data', (data, isNotification) => {
                                        //var string = new TextDecoder("utf-8").decode(data);
                                        if (data.length > 0) {
                                            //const buf = new Buffer(data);
                                            /*var t = byteToHexString(data);*/
                                            /*var reading = buf.readInt8(1);*/
                                            /*var tx = ab2str(buf);*/
                                            //console.log(buf[1], "Heart rate.")
                                            var type = getTemperatureType(data[12]);
                                            var unit = data[3] == 0 ? 'Celsius' : 'Fahrenheit';
                                            var temperatureFloat = (data[2] * 256 + data[1]) / 1000;

                                            measurementData = {
                                                temperature: Math.round(temperatureFloat * 10) / 10,
                                                timestamp: new Date(data[6] * 256 + data[5], data[7], data[8], data[9], data[10], data[11]),
                                                type, unit
                                            };
                                            console.log(measurementData);
                                        }
                                        /*else if (measurementCharacteristic) {
                                         measurementCharacteristic.read((err, data) => {
                                         console.log(err, data, 'Reading');
                                         });
                                         }*/
                                    })
                                    characteristic.subscribe((err) => {
                                        console.log('Subscribed', err);
                                    })
                                    characteristic.read((err, data) => {
                                        console.log(err, data, "READ");
                                    })
                                }
                            /*else if (characteristic.name.indexOf("Interval") != -1) {
                             measurementCharacteristic = characteristic;

                             }*/
                            console.log(characteristic.name)
                        })
                    });

                service.discoverCharacteristics();
            }
        })
    });
    if (peripheral.advertisement.localName == 'BlueSim')
        peripheral.connect((err) => {
            //console.log("Connecing..", err);
            peripheral.discoverServices();
        })
});
noble.on('stateChange', (state) => {
    if (state == 'poweredOn') {
        console.log("Starting scanning.");
        noble.startScanning();
    }
    else
        console.log("State is off.");
});
function ab2str(buf) {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
}
function byteToHexString(uint8arr) {
    if (!uint8arr) {
        return '';
    }

    var hexStr = '';
    for (var i = 0; i < uint8arr.length; i++) {
        var hex = (uint8arr[i] & 0xff).toString(16);
        hex = (hex.length === 1) ? '0' + hex : hex;
        hexStr += hex;
    }

    return hexStr.toUpperCase();
}
function getData() {
    return measurementData
}
function getTemperatureType(typeEnum) {
    switch (typeEnum) {
        case 1:
            return 'Armpit';
            break;
        case 2:
            return 'Body';
            break;
        case 3:
            return 'Ear'
            break;
        case 4:
            return 'Finger'
            break;
        case 5:
            return 'Gastro'
            break;
        case 6:
            return 'Mouth'
            break;
        case 7:
            return 'Rectum'
            break;
        case 8:
            return 'Toe'
            break;
        case 9:
            return 'Typanum (Ear Drum)'
            break;
    }
}
var measurementCharacteristic;
module.exports.getData = getData;